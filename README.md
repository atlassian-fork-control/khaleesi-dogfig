# Datadog resources for Khaleesi #

This repository holds Datadog resources used by the Khaleesi team.

## Installation ##
It is recommended to use python virtualenv's to run this repo locally.
The repository uses dogfig which requires python3 to be installed.

`pip install git+ssh://git@stash.atlassian.com:7997/csre/dogfig.git`

Set your environment variables for Datadog API access.
`export DD_API_KEY=`
`export DD_APP_KEY=`

## Running in a docker container (no dogfig-ddget) ##
Use the DD API and APP key where required. Run dogfig with the overwrite flag to write changes to datadog.
```
docker run --rm -it -v `pwd`:/dogfig  python:3.6.4-alpine3.7 /bin/sh 
pip install dogfig --extra-index-url https://atlassian.jfrog.io/atlassian/api/pypi/conf-sre-pypi-local/simple
export DD_API_KEY=
export DD_APP_KEY=
cd  dogfig
dogfig --overwrite
```

## Running in a docker container (with dogfig-ddget) ##
```
git clone ssh://git@stash.atlassian.com:7997/csre/dogfig.git
cd dogfig
pip install .
export DD_API_KEY=
export DD_APP_KEY=
dogfig-ddget dogfig-ddget https://app.datadoghq.com/dash/123456/some-dashboard > some-dashboard.json 
```


## Adding new Dashboards/Monitors ##
Pull down a dashboard into the templates folder
```
dogfig-ddget https://app.datadoghq.com/dash/123456/some-dashboard > some-dashboard.json
```
Then update the config.yaml file to include it in the overall build

## Note ##
Datadog api can be flaky so usually the monitors upload/overwrite may fail due to 5xx error.
Just run them again to fix.

## Resources ##

* Dogfig: https://stash.atlassian.com/projects/CSRE/repos/dogfig/browse
* Dogfig Example: https://bitbucket.org/identitysre/idsre-dogfig
* Datadog: https://app.datadoghq.com/dash/list